<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\unit_kerja;
use App\klausul_iso;
use App\auditee;
use App\ncr;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class auditorncrController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view("auditor_ncr");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function insert(Request $request)
    {
        //
        // $unit_kerja = $request->input('unit_kerja');
        // $auditee = $request->input('auditee');
        // $kategori = $request->input('kategori');
        // $klausul_iso = $request->input('klausul_iso');
        // $uraian_ktdsn = $request->input('uraian_ktdsn');
        // $bukti_ktdsn = $request->input('bukti_ktdsn');
        // $data=array('unit_kerja'=>$unit_kerja,"auditee"=>$auditee,"kategori"=>$kategori,"klausul_iso"=>$klausul_iso,"uraian_ktdsn"=>$uraian_ktdsn,"bukti_ktdsn"=>$bukti_ktdsn);
        // DB::table('ncr_auditor')->insert($data);
        // echo "Record inserted successfully.<br/>";
        // echo '<a href = "/insert">Click Here</a> to go back.';
        return 'tezst';
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $data['unit_kerja'] = unit_kerja::all();
        $data['klausul_iso'] = klausul_iso::all();
        $data['auditee'] = auditee::all();
        $data['ncr'] = ncr::all();
        //$data['auditor'] = ncr::find(1)->auditor;
        return view('auditor.index')->with(compact('data'));
        // return $select;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
