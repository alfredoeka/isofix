<?php

namespace App\Http\Controllers;

use App\User;
use Validator;
use DB;
use Auth;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

class joinController extends Controller
{
    public function viewrole(){
        $users = DB::table('users')
            ->join('role_user', 'users.role_id_user', '=', 'role_user.id')
            ->select('users.*', 'role_user.role')
            ->where('users.name','=',Auth::user()->name)
            ->get();
        return $users;
    }
}
