<?php

Route::get('/back', function () {
    if (Auth::user()->role_id_user==2) {
		return Redirect::to('/auditor');
	}elseif (Auth::user()->role_id_user==3) {
		return Redirect::to('/auditee');
	}else{
		return Redirect::to('/auth/login');
	}
});
Route::get('/',function(){
	if (Auth::guest()) {
		return Redirect::to('auth/login');
	}else{
		// echo "welcome home admin " . Auth::user()->name. '.';
		if ((Auth::user()->role_id_user)==1) {
			// echo Auth::user()->name"admin";
			return Redirect::to('/admin');
		}elseif ((Auth::user()->role_id_user)==2) {
			# code...
			return Redirect::to('/observation');
		}
	}
})->middleware('auth');


Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::get('coba', 'joinController@viewrole');
Route::post('auth/login', 'Auth\AuthController@postLogin');
Route::get('auth/logout', 'Auth\AuthController@getLogout');

// Registration routes...
Route::get('auth/register', 'Auth\AuthController@getRegister');
Route::post('auth/register', 'Auth\AuthController@postRegister');

//firstpage of admin
Route::get("/admin",function(){
	if (Auth::user()->role_id_user==1) {
		$users = DB::table('users')->get();

		return view('administrator.index', ['users' => $users]);
	}else{
		return Redirect::to('/back');
	}

});


Route::get('/auditor', function () {
	if (Auth::user()->role_id_user==2) {
		return view('auditor.index');
	}else{
		echo "anda admin";
	}
    
})->middleware('auth');;

Route::get('/ncr', function () {
    return view('administrator.ncr');
})->middleware('auth');;

Route::get('/observation', function () {
    return view('administrator.observation');
})->middleware('auth');;

Route::get('/ofi', function () {
    return view('administrator.ofi');
})->middleware('auth');;

// Route::get('/auditor_ncr', function () {
//     return view('auditor.index');
// })->middleware('auth');;
Route::get('/auditor_ncr','auditorncrController@show');
Route::post('/insert','auditorncrController@insert');

Route::get('/auditor_observation', function () {
    return view('auditor.observation');
})->middleware('auth');;


