<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TglRealisasi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tgl_realisasi', function (Blueprint $table) {
            $table->increments('id_tgl_realisasi');
            $table->date('tgl_realisasi');
            $table->integer('verifikasi');
            $table->longText('alasan')->nullable();
            $table->integer('auditor_id');

            $table->foreign('auditor_id')->references('id_auditor')->on('auditor');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
