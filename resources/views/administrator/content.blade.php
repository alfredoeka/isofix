        <div class="content-w">
          <div class="content-panel-toggler">
            <i class="os-icon os-icon-grid-squares-22"></i><span>Sidebar</span>
          </div>
          <div class="content-i">
            <div class="content-box">
              <div class="row">
                <div class="col-sm-12">
                  <div class="element-wrapper">
                    <h6 class="element-header">
                      PERIODE AUDIT
                    </h6>
                    <div class="element-box-tp">
                      <form>
                        <div class="steps-w">
                          <div class="step-contents">
                            <div class="step-content active" id="stepContent1">
                              <div class="col-sm-4">
                                <label for=""> Periode</label><select class="form-control">
                                  <option>
                                    Periode 1
                                  </option>
                                  <option>
                                    Periode 2
                                  </option>
                                  <option>
                                    Periode 3
                                  </option>
                                </select>
                              </div>
                              <div class="col-sm-4">
                                <label for=""> Versi ISO</label><select class="form-control">
                                  <option>
                                    ISO 2008
                                  </option>
                                  <option>
                                    ISO 2015
                                  </option>
                                  <option>
                                    ISO 2018
                                  </option>
                                </select>
                              </div>
                              <div class="controls-above-table">
                              </div>
                            </div>
                          </div>
                        </div>
                      </form>
                      <div class="table-responsive">
                        <table class="table table-bordered table-lg table-v2 table-striped">
                          <thead>
                            <tr>
                              <th>
                                No
                              </th>
                              <th> 
                                NIP
                              </th>
                              <th class="col-sm-8">
                                Nama
                              </th>
                              <th>
                                Lead
                              </th>
                              <th>
                                Auditor
                              </th>
                            </tr>
                          </thead>
                          <tbody>
                            @foreach($users as $key => $data)
                            <tr>
                              <td>
                                
                              </td>
                              <td>
                                {{$data->nip}}
                              </td>
                              <td>
                                {{$data->name}}
                              </td>
                              <td>
                                <label class="switch">
                                  <input type="checkbox">
                                  <span class="slider round"></span>
                                </label>
                              </td>
                              <td>
                                <label class="switch">
                                  <input type="checkbox">
                                  <span class="slider round"></span>
                                </label>
                              </td>
                            </tr>
                            @endforeach
                            
                          </tbody>
                        </table>
                      </div>
                      <div class="controls-below-table">
                        <div class="table-records-info">
                          Showing records 1 - 5
                        </div>
                        <div class="table-records-pages">
                          <ul>
                            <li>
                              <a href="#">Previous</a>
                            </li>
                            <li>
                              <a class="current" href="#">1</a>
                            </li>
                            <li>
                              <a href="#">2</a>
                            </li>
                            <li>
                              <a href="#">3</a>
                            </li>
                            <li>
                              <a href="#">4</a>
                            </li>
                            <li>
                              <a href="#">Next</a>
                            </li>
                          </ul>
                        </div>
                      </div>
                      <div class="form-buttons-w text-right">
                        <button class="btn btn-primary">Selesai</button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>