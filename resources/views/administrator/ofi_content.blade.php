        <div class="content-w">
          <div class="content-panel-toggler">
            <i class="os-icon os-icon-grid-squares-22"></i><span>Sidebar</span>
          </div>
          <div class="content-i">
            <div class="content-box">
              <div class="row">
                <div class="col-sm-12">
                  <div class="element-wrapper">
                    <h6 class="element-header">
                      REKAPITULASI HASIL OBSERVASION FOR IMPROVEMENT (OFI)
                    </h6>
                    <div class="element-box-tp">
                      <div class="controls-above-table">
                        <div class="row">
                          <div class="col-sm-12">
                            <form class="form-inline justify-content-sm-end">
                              <input class="form-control form-control-sm rounded bright" placeholder="Search" type="text"><select class="form-control form-control-sm rounded bright">
                                <option selected="selected" value="">
                                  Select Status
                                </option>
                                <option value="Pending">
                                  Pending
                                </option>
                                <option value="Active">
                                  Active
                                </option>
                                <option value="Cancelled">
                                  Cancelled
                                </option>
                              </select>
                            </form>
                          </div>
                        </div>
                      </div>
                      <div class="table-responsive">
                        <table class="table table-bordered table-lg table-v2 table-striped">
                          <thead>
                            <tr>
                              <th>
                                No
                              </th>
                              <th> 
                                Unit Kerja
                              </th>
                              <th class="col-sm-8">
                                Saran
                              </th>
                              <th>
                                Action
                              </th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <td>
                              </td>
                              <td>
                              </td>
                              <td>
                              </td>
                              <td class="row-actions">
                                <a href="#"><i class="os-icon os-icon-pencil-2"></i></a><a href="#"><i class="os-icon os-icon-link-3"></i></a><a class="danger" href="#"><i class="os-icon os-icon-database-remove"></i></a>
                              </td>
                            </tr>
                            <tr>
                              <td>
                              </td>
                              <td>
                              </td>
                              <td>
                              </td>
                              <td class="row-actions">
                                <a href="#"><i class="os-icon os-icon-pencil-2"></i></a><a href="#"><i class="os-icon os-icon-link-3"></i></a><a class="danger" href="#"><i class="os-icon os-icon-database-remove"></i></a>
                              </td>
                            </tr>
                            <tr>
                              <td>
                              </td>
                              <td>
                              </td>
                              <td>
                              </td>
                              <td class="row-actions">
                                <a href="#"><i class="os-icon os-icon-pencil-2"></i></a><a href="#"><i class="os-icon os-icon-link-3"></i></a><a class="danger" href="#"><i class="os-icon os-icon-database-remove"></i></a>
                              </td>
                            </tr>
                            <tr>
                              <td>
                              </td>
                              <td>
                              </td>
                              <td>
                              </td>
                              <td class="row-actions">
                                <a href="#"><i class="os-icon os-icon-pencil-2"></i></a><a href="#"><i class="os-icon os-icon-link-3"></i></a><a class="danger" href="#"><i class="os-icon os-icon-database-remove"></i></a>
                              </td>
                            </tr>
                            <tr>
                              <td>
                              </td>
                              <td>
                              </td>
                              <td>
                              </td>
                              <td class="row-actions">
                                <a href="#"><i class="os-icon os-icon-pencil-2"></i></a><a href="#"><i class="os-icon os-icon-link-3"></i></a><a class="danger" href="#"><i class="os-icon os-icon-database-remove"></i></a>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                      <div class="controls-below-table">
                        <div class="table-records-info">
                          Showing records 1 - 5
                        </div>
                        <div class="table-records-pages">
                          <ul>
                            <li>
                              <a href="#">Previous</a>
                            </li>
                            <li>
                              <a class="current" href="#">1</a>
                            </li>
                            <li>
                              <a href="#">2</a>
                            </li>
                            <li>
                              <a href="#">3</a>
                            </li>
                            <li>
                              <a href="#">4</a>
                            </li>
                            <li>
                              <a href="#">Next</a>
                            </li>
                          </ul>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>