        <div class="content-w">
          <div class="content-i">
            <div class="content-box">
              <div class="element-wrapper">
                <div class="element-box">
                  <form>
                    <div class="steps-w">
                      <div class="step-triggers">
                        <a class="step-trigger active" href="#stepContent1">First Step</a><a class="step-trigger" href="#stepContent2">Second Step</a><a class="step-trigger" href="#stepContent3">Third Step</a>
                      </div>
                      <div class="step-contents">
                        <div class="step-content active" id="stepContent1">
                          <div class="row">
                            <div class="col-sm-6">
                              <div class="form-group">
                                <label for="responsibility"> Unit Kerja :</label><select class="form-control select2" id="responsibility" name="responsible" single="true">
                                @foreach($data as d)
                                <option>{{$d->unit_kerja}}</option>
                                @endforeach
                              </select>
                              </div>
                            </div>
                            <div class="col-sm-6">
                              <div class="form-group">
                                <div class="form-group">
                                  <label for=""> Auditee :</label><select class="form-control select2" single="true">
                                  <option selected="true">
                                    New York
                                  </option>
                                  <option selected="true">
                                    California
                                  </option>
                                  <option>
                                    Boston
                                  </option>
                                  <option>
                                    Texas
                                  </option>
                                  <option>
                                    Colorado
                                  </option>
                                </select>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="form-group">
                            <label for="">Klausul ISO 9001</label><select class="form-control select2" multiple="true">
                              <option selected="true">
                                New York
                              </option>
                              <option selected="true">
                                California
                              </option>
                              <option>
                                Boston
                              </option>
                              <option>
                                Texas
                              </option>
                              <option>
                                Colorado
                              </option>
                            </select>
                          </div>
                          <div class="form-group">
                            <label> Observasi </label><textarea class="form-control" rows="3" required></textarea>
                          </div>
                          <div class="form-group">
                            <label> Saran / Tindakan Perbaikan : </label><textarea class="form-control" rows="3" required></textarea>
                          </div>
                          <div class="form-buttons-w text-right">
                            <a class="btn btn-primary step-trigger-btn" href="#stepContent2"> Continue</a>
                          </div>
                        </div>
                        <div class="step-content" id="stepContent2">
                          <div class="form-group">
                            <form action="#" method="get">
                              <p>Verifikasi :</p>
                              <p><input type="radio" name="Tindakan" value='diterima'/> Diterima</p>
                              <p><input type="radio" name="Tindakan" value='ditolak'/> Ditolak</p>
                            </form>
                          </div>
                          <div class="form-group">
                            <label> Alasan jika ditolak </label><textarea class="form-control" rows="3"></textarea>
                          </div>
                          <div class="form-buttons-w text-right">
                            <a class="btn btn-primary step-trigger-btn" href="#stepContent3"> Continue</a>
                          </div>
                        </div>
                        <div class="step-content" id="stepContent3">
                          <div class="form-group">
                            <form action="#" method="get">
                              <p>Verifikasi tindakan perbaikan/pencegahan yang diambil :</p>
                              <p><input type="radio" name="Tindakan" value='diterima'/> Diterima</p>
                              <p><input type="radio" name="Tindakan" value='ditolak'/> Ditolak</p>
                            </form>
                          </div>
                          <div class="form-group">
                            <label> Alasan jika ditolak </label><textarea class="form-control" rows="3"></textarea>
                          </div>
                          <div class="form-buttons-w text-right">
                            <button class="btn btn-primary">Submit Form</button>
                          </div>
                        </div>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
    